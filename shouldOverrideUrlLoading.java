/* 指定只有url里包含eoe.cn的时候才在webview里打开，否则还是启动浏览器打开 */
@Override
public boolean shouldOverrideUrlLoading(WebView view, String url) {
	LogUtil.i(this, "url=" + url);
	if ( url.contains("eoe.cn") == true){
		view.loadUrl(url);
		return true;
	}else{
	    Intent in = new Intent (Intent.ACTION_VIEW , Uri.parse(url));
	    startActivity(in);
	    return true;
	}
}